FROM jupyter/scipy-notebook:1386e2046833

RUN conda install --quiet --yes \
    nodejs \
    && \
    conda clean --all -f -y && \
    jupyter labextension install dask-labextension --no-build && \
    npm cache clean --force

RUN pip install dask_labextension

RUN jupyter lab build
